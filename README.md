# stargate

[![pipeline status](https://gitlab.com/cvcf/stargate/badges/master/pipeline.svg)](https://gitlab.com/cvcf/stargate/-/commits/master)
[![coverage report](https://gitlab.com/cvcf/stargate/badges/master/coverage.svg)](https://gitlab.com/cvcf/stargate/-/commits/master)

Generate status reports.

## Requirements

- [Roswell](https://github.com/roswell/roswell/)

## Running

To run the application, execute the following command:

```bash
$ stargate
```

## Testing

To run the test suite, execute the following command:

```bash
$ ros run -e '(asdf:test-system :stargate)'
```

## Example Usage

An example of usage can be found in the ASDF system `:stargate/sample`.  Note
that this is a valid ASDF system that, when loaded, will generate a sample PDF
report.  If desired, you can use this as a starting point for your own needs.

To generate the sample report, execute the following command:

```bash
$ ros run -e '(asdf:load-system :stargate/sample)'
```

## License

Copyleft (ↄ) 2020, Cameron Chaparro <cameron@cameronchaparro.com>.

This work is licensed under the terms of the GNU General Public License version 3.
