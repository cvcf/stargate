(defpackage #:stargate/sample
  (:use #:cl #:stargate))

(in-package #:stargate/sample)

(setf *transformations* (list (define-line-transform "\(.*\)@\(\\w+\)\.\(\\w+\)\(.*\)" "~a~:(~a~) ~:(~a~)~a")
                              (define-line-transform "\(\[\\s\\d\.\]+\)\(.\)\(.*\)" "~a~:(~a~)~a")
                              (define-line-transform "\(.*\)" "~a.")))

(gen-status-report (:filespec "sample.pdf"
                    :title    "Sample Report"
                    :author   "T. Author"
                    :subject  "A sample report for you"
                    :keywords "sample,report")

  (gen-report-table (:col-widths '(100 100 50 60 70) :cell-padding 5)

    (gen-report-table-heading "Sample Report Heading"
      :height 30
      :background-color #xfefea6)

    (gen-report-table-subheading "Sample subheading"
      :h-align :left
      :background-color #xccffff)

    (gen-report-table-rows-from-files-in (asdf:system-relative-pathname :stargate/sample "sample/report/"))))
