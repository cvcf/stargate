(defpackage #:stargate-test/utils
  (:use #:cl #:cl-fad #:cl-ppcre #:stargate)
  (:export #:grep
           #:rgrep))

(in-package #:stargate-test/utils)

(defun rgrep (re dir)
  (let ((matches))
    (walk-directory dir
                    (lambda (file)
                      (setf matches (nconc matches (grep re file)))))
    matches))

(defun grep (re file)
  (with-file-contents file
    (all-matches-as-strings re contents)))
