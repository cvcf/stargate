(defpackage #:stargate-test/unit
  (:use #:cl #:stargate #:stargate-test/utils #:prove))

(in-package #:stargate-test/unit)

#+swank
(setf *enable-colors* nil)
#-swank
(setf *default-reporter* :tap)

(defpackage #:stargate-test/integration
  (:use #:cl #:cl-fad #:stargate #:stargate-test/utils #:prove))

(in-package #:stargate-test/integration)

#+swank
(setf *enable-colors* nil)
#-swank
(setf *default-reporter* :tap)

(defpackage #:stargate-test/functional
  (:use #:cl #:cl-fad #:cl-ppcre #:tt #:stargate #:stargate-test/utils #:prove))

(in-package #:stargate-test/functional)

#+swank
(setf *enable-colors* nil)
#-swank
(setf *default-reporter* :tap)

(in-package #:cl-user)
