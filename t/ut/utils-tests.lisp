(in-package #:stargate-test/unit)

(plan 1)

(subtest "trim-lambda-list"
  (let ((vals '(:one 1 :two 2 :three 3)))
    (subtest "value is removed when it exists"
      (is (sg::trim-lambda-list vals :two)
          '(:one 1 :three 3))
      (is vals '(:one 1 :two 2 :three 3)))

    (subtest "no change when value does not exist"
      (is (sg::trim-lambda-list vals :four) vals))))

(finalize)
