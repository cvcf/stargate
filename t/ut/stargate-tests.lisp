(in-package #:stargate-test/unit)

(plan 2)

(let ((lines '("line that doesn't match anything"
               "line that matches the cl acronym"
               "line with a name: @john.doe"
               "line without matches, again"))
      (*transformations* (list (define-line-transform "\(.*\)@\(.*\)\\.\(.*\)\(.*\)" "~a~:(~a~) ~:(~a~)~a")
                               (define-line-transform "\(.*\)\( cl \)\(.*\)" "~a~:@(~a~)~a"))))

  (subtest "define-line-transform"
    (ok (every (lambda (x) (is-type x 'function)) *transformations*)))

  (subtest "transform-lines"
    (let ((transformed (transform-lines lines)))
      (is (length transformed) 4)
      (is (second transformed) "line that matches the CL acronym")
      (is (third transformed)  "line with a name: John Doe"))))

(finalize)
