(in-package #:stargate-test/integration)

(plan 1)

(subtest "with-file-contents"
  (let ((text (format nil "here we~%have some~%file contents"))
        (name))
    (with-open-temporary-file (stream :keep t)
      (format stream text)
      (setf name (pathname stream)))
    (with-file-contents name
      (is contents text))))

(finalize)
