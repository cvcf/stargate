(in-package #:stargate-test/functional)

(defmacro contains-p (val items)
  `(member (format nil "/~:(~a~) (~a)" ,(string val) ,val) ,items :test #'string=))

(plan 2)

(let ((filespec "/tmp/ft.pdf")
      (author   "test author")
      (title    "test title")
      (subject  "test subject")
      (keywords "functional,test,keywords")
      (report-dir (asdf:system-relative-pathname :stargate "sample/report/")))

  (let ((doc (gen-status-report (:filespec filespec
                                 :author   author
                                 :title    title
                                 :subject  subject
                                 :keywords keywords)

               (paragraph ()
                 (put-string "test document"))

               (gen-report-table (:col-widths '(100 100))
                 (gen-report-table-heading "test heading")
                 (gen-report-table-subheading "test subheading")
                 (gen-report-table-cells () '("cells one" "cells two"))
                 (gen-report-table-cell () "test title" '("cell one" "cell two"))))))

    (subtest "basic document generation"
      (is doc (file-exists-p filespec))
      (with-file-contents doc
        (ok (plusp (length contents)))
        (is (length (all-matches-as-strings "(cells?).*(one)" contents)) 2)
        (is (length (all-matches-as-strings "(cells?).*(tw).*(o)" contents)) 2)

        (let ((metadata (all-matches-as-strings "/(Author|Title|Subject|Keywords) (.*)" contents)))
          (ok (contains-p author   metadata))
          (ok (contains-p title    metadata))
          (ok (contains-p subject  metadata))
          (ok (contains-p keywords metadata)))))

    (delete-file filespec))

  (let ((doc (gen-status-report (:filespec filespec)
               (gen-report-table (:col-widths '(100 100))
                 (gen-report-table-rows-from-files-in report-dir))))
        (todo-done-re "[tTdD][\\W\\d]*[oO][\\W\\d]*[dDnN][\\W\\d]*[oOeE]"))
    (subtest "document generation from files in a directory"
      (is doc (file-exists-p filespec))
      (is (length (grep todo-done-re doc))
          (length (rgrep todo-done-re report-dir))))

    (delete-file filespec)))

(finalize)
