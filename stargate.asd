(defsystem #:stargate
  :description "Generate status reports."
  :version (:read-file-form "variables" :at (0 1))
  :author "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :maintainer "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :homepage "https://gitlab.com/cvcf/stargate"
  :bug-tracker "https://gitlab.com/cvcf/stargate/-/issues"
  :license "GPLv3"
  :depends-on (:cl-fad :cl-ppcre :cl-typesetting :split-sequence)
  :serial t
  :pathname "src/"
  :components ((:file "packages")
               (:file "utils")
               (:file "stargate"))
  :in-order-to ((test-op (test-op #:stargate-test))))

(defsystem #:stargate/sample
  :description "Generate a very simple sample report using stargate."
  :depends-on (:stargate)
  :serial t
  :pathname "sample/"
  :components ((:file "gen-sample")))
