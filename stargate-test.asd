(defsystem #:stargate-test
  :description "Test system for the stargate project"
  :depends-on (:cl-fad
               :cl-ppcre
               :cl-typesetting
               :prove
               :stargate
               :stargate-test/unit
               :stargate-test/integration
               :stargate-test/functional))

(defsystem #:stargate-test/unit
  :description "Unit test system for stargate"
  :depends-on (:stargate :prove)
  :serial t
  :pathname "t/"
  :components ((:file "utils")
               (:file "packages")
               (:module "ut"
                :components ((:file "utils-tests")
                             (:file "stargate-tests")))))

(defsystem #:stargate-test/integration
  :description "Integration test system for stargate"
  :depends-on (:cl-fad :stargate :prove)
  :serial t
  :pathname "t/"
  :components ((:file "utils")
               (:file "packages")
               (:module "it"
                :components ((:file "utils-tests")
                             (:file "stargate-tests")))))

(defsystem #:stargate-test/functional
  :description "Functional test system for stargate"
  :depends-on (:cl-fad :cl-ppcre :cl-typesetting :stargate :prove)
  :serial t
  :pathname "t/"
  :components ((:file "utils")
               (:file "packages")
               (:module "ft"
                :components ((:file "stargate-tests")))))
