(in-package #:stargate)

(defparameter *transformations* nil)

(defparameter +default-page-margins+ '(20 40 20 40))
(defparameter +default-page-size+ (rest (assoc :letter tt::+paper-sizes+)))

(defmacro gen-status-report ((&key
                                filespec title author subject keywords
                                (margins +default-page-margins+)
                                (size +default-page-size+))
                             &body body)
  (let ((content (gensym))
        (pathspec (gensym)))
    `(let ((,pathspec (pathname ,filespec)))
       (with-document (:title    ,title
                       :author   ,author
                       :subject  ,subject
                       :keywords ,keywords)
        (let ((,content (compile-text () ,@body)))
          (draw-pages ,content :margins ',margins :size ',size))
        (finalize-page *page*)
        (write-document ,pathspec))
       ,pathspec)))

(defmacro gen-report-table ((&rest style
                             &key (top-margin 10) (bottom-margin 10)
                             &allow-other-keys)
                            &body body)
  (let ((style (trim-lambda-list style :top-margin :bottom-margin)))
    `(progn
       (vspace ,top-margin)
       (table ,style
         ,@body)
       (vspace ,bottom-margin))))

(defmacro gen-report-table-heading (text &body style
                                    &key
                                      (font-size 18)
                                      (font-family "Helvetica")
                                      (h-align :center)
                                      (v-align :center)
                                    &allow-other-keys)
  (let ((style (trim-lambda-list style :font-size :font-family :h-align :v-align)))
    `(header-row ,style
       (cell (:v-align ,v-align :col-span 0)
         (with-style (:font-family ,font-family
                      :font-size   ,font-size
                      :h-align     ,h-align)
           (put-string ,text))))))

(defmacro gen-report-table-subheading (text &body style
                                       &key
                                         (font-size 14)
                                         (font-family "Helvetica")
                                         (h-align :center)
                                         (v-align :center)
                                       &allow-other-keys)
  (let ((style (trim-lambda-list style :font-size :font-family :h-align :v-align)))
    `(row ,style
       (cell (:v-align ,v-align :col-span 0)
         (with-style (:font-family ,font-family
                      :font-size   ,font-size
                      :h-align     ,h-align)
           (put-string ,text))))))

(defmacro gen-report-table-cell ((&rest style &key (v-align :center) (indent 5) &allow-other-keys) title &body lines)
  (let ((style (trim-lambda-list style :v-align :indent))
        (line (gensym)))
    `(row ,style
       (cell (:v-align ,v-align :col-span 0)
         (paragraph ()
           (hspace ,indent)
           (put-string ,title)
           (loop :for ,line :in ,@lines
                 :collect (progn
                            (new-line)
                            (hspace ,(* 3 indent))
                            (put-string ,line))))))))

(defmacro gen-report-table-cells ((&rest style &key (v-align :center) (indent 5) &allow-other-keys) &body lines)
  (let ((style (trim-lambda-list style :v-align :indent))
        (line (gensym)))
    `(row ,style
       (loop :for ,line :in ,@lines
             :collect (cell (:v-align ,v-align)
                        (hspace ,indent)
                        (put-string ,line))))))

;; TODO(Cameron): this is not how we should *generically* handle setting the row
;; title but, for now, it makes sense to do it this way
(defmacro gen-report-table-row-from-file (filespec &rest style)
  (let ((lines (gensym))
        (title (gensym))
        (body  (gensym)))
    `(with-file-contents ,filespec
       (let ((,lines (split-sequence #\Newline contents)))
         (let ((,title (first ,lines))
               (,body  (transform-lines (rest ,lines))))
           (gen-report-table-cell ,style ,title ,body))))))

(defmacro gen-report-table-rows-from-files-in (dirspec &rest style)
  (let ((filespec (gensym)))
    `(walk-directory ,dirspec
                     (lambda (,filespec)
                       (gen-report-table-row-from-file ,filespec ,@style)))))

(defun define-line-transform (re fmt-string)
  (lambda (line)
    (let ((scanner (create-scanner re :extended-mode t)))
      (multiple-value-bind (i j beg end) (funcall scanner line 0 (length line))
        (declare (ignore i j))
        (let ((fmt-args (loop :for k :from 0 :to (1- (length beg))
                              :collect (subseq line (aref beg k) (aref end k)))))
          (if (null fmt-args)
              (format nil "~a" line)
              (eval `(format nil ,fmt-string ,@fmt-args))))))))

(defun transform-lines (lines)
  (let ((results))
    (dolist (transform *transformations* lines)
      (setf results nil)
      (setf lines
            (dolist (line lines (nreverse results))
              (push (eval (funcall transform line)) results))))))
