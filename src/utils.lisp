(in-package #:stargate)

(defun trim-lambda-list (vals &rest args)
  (let ((vals (copy-list vals)))
    (dolist (arg args vals)
      (remf vals arg))))

(defmacro with-file-contents (filespec &body body)
  (let ((fs (gensym))
        (str (gensym))
        (line (gensym)))
    `(with-open-file (,fs ,filespec)
       (let ((,str (make-string-output-stream)))
         (loop :for ,line := (read-line ,fs nil nil)
               :while ,line
               :do (format ,str "~&~a" ,line))
         (let ((contents (get-output-stream-string ,str)))
           ,@body)))))
