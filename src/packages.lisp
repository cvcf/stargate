(defpackage #:stargate
  (:use #:cl #:tt)
  (:nicknames #:sg)
  (:import-from #:pdf
                #:*page*)
  (:import-from #:cl-fad
                #:walk-directory)
  (:import-from #:split-sequence
                #:split-sequence)
  (:import-from #:cl-ppcre
                #:create-scanner)
  (:export #:gen-status-report
           #:gen-report-table
           #:gen-report-table-heading
           #:gen-report-table-subheading
           #:gen-report-table-cell
           #:gen-report-table-cells
           #:gen-report-table-row-from-file
           #:gen-report-table-rows-from-files-in

           ;; line transformations
           #:*transformations*
           #:define-line-transform
           #:transform-lines

           ;; utils
           #:with-file-contents
           #:contents))
